 第一步：修改项目名
打开docker-compose.yml和docker目录下的所有文件
然后把MyApp全局替换为你的项目名（比如 MyApp -> fiesta）
然后把你的项目整个放置于 /opt/www/ 目录下


第二步：给Laravel安装Swoole插件：

文档：https://github.com/swooletw/laravel-swoole  

```
cd /opt/www/fiesta
COMPOSER_MEMORY_LIMIT=-1 composer require swooletw/laravel-swoole
php artisan vendor:publish --tag=laravel-swoole

```
 
然后打开 config/swoole_http.php
注释掉下面2行：
```
//'socket_type' => SWOOLE_SOCK_TCP,
//'process_type' => SWOOLE_PROCESS,
```

在 .env 文件里添加下面几行：

```
SWOOLE_HTTP_HOST=0.0.0.0
SWOOLE_HTTP_DAEMONIZE=false
SWOOLE_HTTP_PORT=5000
SWOOLE_HTTP_WEBSOCKET=on
```


第三步：更新Laravel缓存

```
bash reload.sh
```


第四步：启动项目

```
docker-compose up 
```

如果要关闭项目：
```
docker-compose down
```

如果没有错误，那么访问 127.0.0.1 就能正常打开网页了

可选：如果需要websocket，可以让JS代码连接到 127.0.0.1:9000