#!/bin/bash

php artisan view:clear
php artisan event:clear

composer dump-autoload --optimize

php artisan clear-compiled
php artisan optimize

php artisan route:cache
php artisan config:cache

